+++
date = "2017-03-11T11:23:43+01:00"
publishdate = "2017-03-11T11:23:43+01:00"
title = "About"
description = "Information about myself"
menu = "main"

+++

## About Me

I'm Valentin, a computer science student from Regensburg, Germany. Currently I'm studying at [OTH Regensburg][1].

My interests are mainly in IT security and open source software but also practical cryptography and blockchain based technologies. I also like administrating Linux machines and planing network infrastructures.
In my free time I like to play around with binary analysis and exploiting techniques by solving wargames.

I'm coding since ~2010.

Currently working at [EBSnet][2].

## Skills

 * Linux
 * Java EE and SE
 * C
 * Perl 5
 * A bit Rust and Python
 * Bash
 * MySQL
 * Oracle SQL

## System/Tools

 * OS: [Arch Linux][3]
 * WM: [i3wm][4]
 * Editor: [Vim][5]
 * Shell: [Zsh][6] with [oh-my-zsh][7] and [antigen][8] in [tmux][9]

Also refer to my [dotfiles][10]

## GitHub

Visit [my GitHub profile][11].

## Services

I'm running a [SKS keyserver][13] on this server: [keyserver.vbrandl.net][14].

The [website][15] and [keyserver][16] are also available via Tor onion services.

## License

If not stated otherwise, every page on this website is released unter the [CC-BY-SA-4.0][12] license.

[1]: https://www.oth-regensburg.de/
[2]: http://ebsnet.de/
[3]: https://archlinux.org
[4]: https://i3wm.org/
[5]: http://www.vim.org
[6]: https://www.zsh.org/
[7]: https://github.com/robbyrussell/oh-my-zsh
[8]: http://antigen.sharats.me/
[9]: https://tmux.github.io/
[10]: https://github.com/vbrandl/dotfiles
[11]: https://github.com/vbrandl
[12]: https://github.com/vbrandl/vbrandl.net/blob/master/LICENSE
[13]: https://sks-keyservers.net/
[14]: https://keyserver.vbrandl.net/
[15]: http://womux7pjybmp6i5q.onion/
[16]: http://yevybz7bh2ge5pct.onion/
