+++
date = "2017-03-11T11:38:41+01:00"
publishdate = "2017-03-11T11:38:41+01:00"
title = "Contact"
description = "My contact information"
menu = "main"

+++

You can contact me on different mediums:

 * Mail: <hi@vbrandl.net> (Please consider using my [GPG key][0]. You can also write a mail to every alias found in the GPG key)
 * [Matrix][1]: @vbrandl:matrix.vsund.de


[0]: /static/keys/0x1FFE431282F4B8CC0A7579167FB009175885FC76.asc
[1]: https://matrix.org/
